package com.example.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import data.RecyclerAdapter;

import static data.Country.countries;

public class DetailFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.detail_fragment, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int id_country = args.getCountryId();


        TextView detail_name = view.findViewById(R.id.detail_name);
        TextView detail_capital = view.findViewById(R.id.detail_capital);
        TextView detail_langue = view.findViewById(R.id.detail_langue);
        TextView detail_monnaie = view.findViewById(R.id.detail_monnaie);
        TextView detail_population = view.findViewById(R.id.detail_population);
        TextView detail_surface = view.findViewById(R.id.detail_surface);
        ImageView detail_Image = view.findViewById(R.id.detail_image);

        Context context = detail_Image.getContext();

        detail_name.setText(countries[id_country].getName());

        detail_capital.setText(countries[id_country].getCapital());
        detail_langue.setText(countries[id_country].getLanguage());
        detail_monnaie.setText(countries[id_country].getCurrency());
        detail_population.setText(countries[id_country].getPopulation() + "");
        detail_surface.setText(countries[id_country].getArea() + "");

        detail_Image.setImageDrawable(
                context.getResources().getDrawable(context.getResources().getIdentifier( countries[id_country].getImgUri(), null, context.getPackageName() ))
            );

        view.findViewById( R.id.detail_return ).setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                Navigation.findNavController(view).navigate(R.id.action_SecondFragment_to_FirstFragment );
            }
        });
    }


}