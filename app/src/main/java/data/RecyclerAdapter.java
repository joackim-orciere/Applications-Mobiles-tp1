package data;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp1.DetailFragment;
import com.example.tp1.DetailFragmentArgs;
import com.example.tp1.ListFragment;
import com.example.tp1.ListFragmentDirections;
import com.example.tp1.R;

import static data.Country.countries;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>
{


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate( R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        viewHolder.itemTitle.setText(countries[i].getName());
        viewHolder.itemDetail.setText(countries[i].getCapital());
        // viewHolder.itemImage.setImageResource(countries[i].getImgUri());

        String uri = countries[i].getImgUri();
        Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));

    }

    @Override
    public int getItemCount()
    {
        return countries.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemImage = itemView.findViewById(R.id.image);
            itemTitle = itemView.findViewById(R.id.name);
            itemDetail = itemView.findViewById(R.id.capital);


            itemView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {

                    int position = getAdapterPosition();

                    Bundle bundle = new Bundle();
                    bundle.putInt("country_id", position);

                    DetailFragmentArgs detailFragmentArgs = DetailFragmentArgs.fromBundle(bundle);
                    int country_id = detailFragmentArgs.getCountryId();

                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action =
                            ListFragmentDirections.actionFirstFragmentToSecondFragment(country_id);
                    action.setCountryId( country_id );

                    Navigation.findNavController(view).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle );

                }
            });

        }
    }

}